#ifndef USUARIO_H
#define USUARIO_H

typedef struct{
    char nombre[10];
    char contrasena[10];
    int edad;
}Usuario;

Usuario* nuevoUser(char *nombre, char *contrasena, int edad);

void editarUser(Usuario *n, int nedad);

void borrarUser(Usuario *n);

void escribirUser(Usuario *n);

#endif