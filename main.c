
#include <stdio.h>
#include <stdlib.h>
#include "usuario.h"
#include "interfaz.h"


int main (int argc, char** argv){

    Usuario * servidor[20];
    char nombre, dni;
    int edad, op=0, nt=0, posicion;

    do{
        op=menu();

        switch (op){
            case NUEVO:
                if (nt<20){
                    printf("Nombre: ");
                    scanf("%s", &nombre);
                    printf("dni: ");
                    scanf("%s", &dni);
                    printf("Edad: ");
                    scanf("%i", &edad);

                    servidor[nt]=nuevoUsuario(nombre, dni, edad);
                    n++;
                }else{
                    printf("Ya no se pueden dar de alta mas usuarios");
                }
                break;
            case EDITAR:
                printf("nueva edad del usuario");
                scanf("%i", &edad);
                printf("ubicacion del usuario en disco");
                scanf("%i", posicion);

                editarUsuario(servidor[posicion],edad);
                break;
            case BORRAR:
                printf("Ubicación en la que se encuentra el usuario");
                scanf("%i", posicion);

                borrarUsuario(servidor[posicion]);
                break;
            case LISTA:
                for(int i=0; i<20; i++)
                    escribirUsuarios(servidor[i]);
                break;
            case SALIR: printf("Saliendo del programa");
                    break;
        }

    }while (op != 0);
}